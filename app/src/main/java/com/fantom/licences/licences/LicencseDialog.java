package com.fantom.licences.licences;

import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.fantom.licences.licences.databinding.LicencseDialogBinding;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;

public class LicencseDialog extends AppCompatDialogFragment {

    private LicencseDialogBinding mBinding;
    private PublishSubject<String> enteredLicencseSubject = PublishSubject.create();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.licencse_dialog, null, false);

        AlertDialog alertDialog = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme)
                .setPositiveButton("Validate", null)
                .setCancelable(false)
                .setView(mBinding.getRoot())
                .create();

        alertDialog.setCanceledOnTouchOutside(false);

        //Prevent dialog from closing itself
        alertDialog.setOnShowListener(dialog -> {
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v -> {
                mBinding.progress.setVisibility(View.VISIBLE);
                enteredLicencseSubject.onNext(mBinding.licenseNumber.getText().toString());
            });
        });

        return alertDialog;
    }

    public PublishSubject<String> getEnteredLicencseSubject() {
        return enteredLicencseSubject;
    }

    public void onLicenceValidationEnded(String licenceType) {
        mBinding.progress.setVisibility(View.GONE);
        if (LicencesUtil.isCorrectLicence(licenceType)) {
            Toast.makeText(getContext(), R.string.correctLicenceMsg, Toast.LENGTH_SHORT).show();
            getDialog().dismiss();
        } else {
            Toast.makeText(getContext(), R.string.invalidLicenceMsg, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
