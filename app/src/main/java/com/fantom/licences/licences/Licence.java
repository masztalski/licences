package com.fantom.licences.licences;

public class Licence {
    private String licenceType;
    private String licenceNumber;

    public Licence(String licenceType, String licenceNumber) {
        this.licenceType = licenceType;
        this.licenceNumber = licenceNumber;
    }

    public String getLicenceType() {
        return licenceType;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }
}
