package com.fantom.licences.licences;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.fantom.licences.licences.databinding.ActivityMainBinding;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private static final String licenseSharedPrefKey = "licence";

    private ActivityMainBinding mainBinding;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        sharedPreferences = getSharedPreferences("Licences", 0);

        showLicencePrompt();
    }

    private void showLicencePrompt() {
        LicencseDialog licenceDialog = new LicencseDialog();
        compositeDisposable.add(licenceDialog.getEnteredLicencseSubject()
                .subscribe(enteredLicence -> {
                    //Received input from user
                    Observable.fromCallable(() -> {
                        //Potentialy long running task of checking licence
                        Log.d("Licence", "Checking licence on:" + Thread.currentThread());
                        return LicencesUtil.checkLicenseType(enteredLicence);
                    })
                            .subscribeOn(Schedulers.computation())
                            .observeOn(Schedulers.io())
                            .doOnNext(licenceType -> {
                                //Writing to storage on io thread
                                Log.d("Licence", "Writing to storage on :" + Thread.currentThread());
                                if (LicencesUtil.isCorrectLicence(licenceType)) {
                                    sharedPreferences.edit().putString(licenseSharedPrefKey, enteredLicence).apply();
                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(licenceType -> {
                                //Operating on view on UI Thread
                                Log.d("Licence", "Received licence on view:" + Thread.currentThread());
                                if (LicencesUtil.isCorrectLicence(licenceType)) {
                                    mainBinding.licenseMessage.setVisibility(View.VISIBLE);
                                    mainBinding.licenseMessage.setText(String.format(getString(R.string.obtainedLicenceType), licenceType));
                                }
                                licenceDialog.onLicenceValidationEnded(licenceType);
                            });
                }));
        licenceDialog.show(getSupportFragmentManager(), "LicenceDialog");
    }
}
