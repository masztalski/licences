package com.fantom.licences.licences;

import java.util.Arrays;
import java.util.List;

public class LicencesUtil {
    public static final String invalidLicense = "INVALID";

    private static final Licence limited = new Licence("LIMITED", "1234");
    private static final Licence pro = new Licence("PRO", "5678");
    private static final List<Licence> avaibleLicences = Arrays.asList(limited, pro);

    public static String checkLicenseType(String licenseNumber){
        for (Licence licence : avaibleLicences){
            if (licence.getLicenceNumber().equals(licenseNumber)){
                return licence.getLicenceType();
            }
        }
        return invalidLicense;
    }

    public static boolean isCorrectLicence(String returnedLicenceType){
        return !returnedLicenceType.equals(invalidLicense);
    }


}
